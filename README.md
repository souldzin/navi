# Navi

An AI for fun conversation

## Requirements?

- [pipenv](https://github.com/pypa/pipenv)

## How to run?

```
pipenv install
pipenv run python -m navi.main
```

## How to use?

Enter `!help` for help using the CLI.
