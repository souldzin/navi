from navi.repl.navi import NaviRepl
from navi.repl.system import SystemRepl
from navi.repl.printer import create_printer


def keep_going():
    return True


def main():
    repls = [SystemRepl(create_printer("system")),
             NaviRepl(create_printer("navi"))]

    while keep_going():
        msg = input("> ")
        for repl in repls:
            if repl.read(msg):
                break


if __name__ == "__main__":
    main()
