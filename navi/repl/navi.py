import random

NAVI_MESSAGES = [
    "Hello!",
    "Hey!",
    "Listen!",
    "Watch out!"
]


class NaviRepl:
    def __init__(self, printer):
        self.printer = printer

    def read(self, msg):
        self.printer(self._get_message())
        return True

    def _get_message(self):
        return random.choice(NAVI_MESSAGES)
