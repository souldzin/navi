import sys

SYSTEM_EXIT_COMMANDS = ['quit', 'q']
SYSTEM_HELP_COMMANDS = ['help', 'h']
SYSTEM_LEADER = "!"


class SystemRepl:
    def __init__(self, printer):
        self.printer = printer

    def read(self, msg):
        if len(msg) and msg[0] == SYSTEM_LEADER:
            self._read_command(msg[1:])
            return True
        else:
            return False

    def _read_command(self, command):
        if command in SYSTEM_EXIT_COMMANDS:
            self._exit()
        elif command in SYSTEM_HELP_COMMANDS:
            self._help()
        else:
            self._unknown_command()

    def _help(self):
        msg = """You are talking to Navi! 
        
Try saying anything and see what she responds with ¯\\_(ツ)_/¯

System commands:
  - !quit (!q) - Quit the program.
  - !help (!h) - Show this message."""

        self.printer(msg)

    def _exit(self):
        self.printer('Goodbye!')
        sys.exit()

    def _unknown_command(self):
        self.printer(
            f'Unknown system command. Try "{SYSTEM_LEADER}{SYSTEM_HELP_COMMANDS[0]}" to see a list of commands.')
