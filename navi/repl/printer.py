def create_printer(name):
    def printer(msg):
        print(F'{name}: {msg}')
    return printer
