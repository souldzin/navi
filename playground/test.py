import nltk

tokens = ['i', 'talk']

g = nltk.data.load('grammars/large_grammars/atis.cfg')
print("Loaded data...")

parser = nltk.parse.BottomUpChartParser(g)
print("Created parser...")

trees = list(parser.parse(tokens))
print("parsed!")
print(len(trees))

for tree in trees:
    print(tree)

